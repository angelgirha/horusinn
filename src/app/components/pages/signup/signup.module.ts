import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SignupRoutingModule } from './signup-routing.module';
import { SignupComponent } from './signup.component';
import {InputTextModule} from 'primeng/inputtext';
import {CheckboxModule} from 'primeng/checkbox';
import {ButtonModule} from 'primeng/button';
import {RippleModule} from 'primeng/ripple';
import {PasswordModule} from 'primeng/password';
import {FormsModule} from '@angular/forms';
import {FileUploadModule} from 'primeng/fileupload';
import {LoadingModule} from '../../shared/loading/loading.module';
import {ToastModule} from 'primeng/toast';


@NgModule({
  declarations: [SignupComponent],
  imports: [
    CommonModule,
    SignupRoutingModule,
    InputTextModule,
    CheckboxModule,
    ButtonModule,
    RippleModule,
    PasswordModule,
    FormsModule,
    FileUploadModule,
    LoadingModule,
    ToastModule
  ]
})
export class SignupModule { }
