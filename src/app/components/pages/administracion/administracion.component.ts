import { Component, OnInit } from '@angular/core';
import { AmbientesService } from 'src/app/services/ambientes.service';
import { ConexionesModel, ServidorModel, ClienteDir } from '../../../models/servidor/servidor.model';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ConfirmationService, MessageService } from 'primeng/api';
import { Router } from '@angular/router';
import { ServidorService } from '../../../services/servidor.service';

@Component({
  selector: 'app-administracion',
  templateUrl: './administracion.component.html',
  styleUrls: ['./administracion.component.css'],
  providers: [MessageService, ConfirmationService]
})
export class AdministracionComponent implements OnInit {

  //listas a usar
  servidorList: ServidorModel[] = [];
  insertServidor: ServidorModel;
  updateServidor: ServidorModel;
  updateServidorO: ServidorModel;
  brands: any[];
  productAsistencia: any[];
  opcionSsh: any[];
  dbList: any[];
  conexiones_disp: any[];
  cancelar = false;
  recorrer: ClienteDir[] = []; //funcion hecha por herby

  private cambios = [];
  search = "";

  //columnas a usar en los formularios
  cols: any[];
  selectedColumns: any[];

  //lista de servidores que sean seleccionados en el front
  selectedServidor: any;

  //
  formulario: true;
  insert = true;
  display = true;
  loading = false;
  errorGral = false;
  msj = '';

  servidorForm: FormGroup;

  productoInsert = undefined;
  tipoPassSsh = undefined;
  conexiones: string[] = [];
  conexionesO: string[] = [];

  // Bloqueos de botones de acciones
  updateTableButtonStatus: boolean = true;
  updateDialogButtonStatus: boolean = true;
  deleteButtonStatus: boolean = true;
  insertButtonStatus: boolean = true;
  cerrarInsertButtonStatus: boolean = false;

  //validaciones para el usuario logeado
  userCanRead: any;
  userCanEdit: any;
  userCanDelete: any;
  userCanLoad: any;
  userCanInsert: any;

  currentRoute: any;
  userPermissions: any[];

  constructor(private fb: FormBuilder,private ambientesServices:AmbientesService , private messageService: MessageService, private router: Router,
    private servidorService: ServidorService, private confirmationService: ConfirmationService) {

    
    this.loading = true;
    this.currentRoute = this.router.url;
    this.servidorService.getAllServers()
      .subscribe(resp => {
        this.loading = false;
        this.servidorList = resp;
      });

    this.brands = [
      { label: 'Girha', value: 'girha' },
      { label: 'Mi Asistencia', value: 'mi_asistencia' },
      { label: 'BioTime Cloud', value: 'biotime_cloud' }
    ];
    this.productAsistencia = [
      { label: 'Mi asistencia', value: 'mi_asistencia' },
      { label: 'Mi asistencia by hik', value: 'mi_asistencia_hik' },
      { label: 'CloudClock', value: 'cloudclock' },
      { label: 'ZKCheckTime', value: 'zkchecktime' }
    ]
    this.opcionSsh = [
      { label: 'Password', value: 'password' },
      { label: 'Key', value: 'key' }];
    this.tipoPassSsh =
      { label: 'Password', value: 'password' };


    this.conexiones_disp = [
      { label: 'MySql', value: 'MySql' },
      { label: 'PostgreSql', value: 'PostgreSql' },
      { label: 'SSH', value: 'SSH' }
    ];

    this.getcliente_dirreccion();

  }

  callDB(valu, produc, dato = '', subpro = '', req = 1){
    var val = { id: valu, nombre: produc, producto: dato, req: req, subproducto: subpro};
    this.servidorService.obtenerBD(val)
      .subscribe(respDB => {
        this.validador();
        return this.dbList = respDB;
      });
  }

  ngOnInit(): void {
    this.userCanRead = 1;
    this.userCanEdit = 1;
    this.userCanDelete = 1;
    this.userCanLoad = 1;
    this.userCanInsert = 1;

    this.servidorForm = this.fb.group({
      'producto': new FormControl('', Validators.required),
      'nombre': new FormControl('', Validators.required),
      'ip_publica': new FormControl('', Validators.required),
      'ip_local': new FormControl('', Validators.required),
    });

    this.cols = [
      { field: 'estatus', header: 'Estatus'},
      { field: 'ambiente', header: 'Ambiente' },
      { field: 'fecha_creacion', header: 'Fecha de Creación' },
      { field: 'Fechadelete', header: 'Fecha de Destrución'},
      { field: 'rfc', header: 'RFC' },
      { field: 'razon', header: 'Razón Social' },
      { field: 'Direccion', header: 'Dirección' },
      { field: 'Condiciones', header: 'Condiciones Días' },
      { field: 'ContactoComer', header: 'Contacto Comercial'},
      { field: 'ContactoFunci', header: 'Contacto Funcional'},
      { field: 'ContactoTecni', header: 'Contacto Técnico'},
    ];

    this.selectedColumns = this.cols;

  }

  buscarDatos() { }

  //formulario de nuevo servidor
  callFormInsert() {

    this.insertServidor = new ServidorModel();
    this.insertServidor.servers = [];
    this.display = true;
    this.formulario = true;
  }


  getcliente_dirreccion(){
    
    this.loading = true;
    this.ambientesServices.getclientes_direccion().subscribe(
      resp => {
        this.recorrer = resp;
        console.log('**************');
        console.log(this.recorrer);
        console.log('**************');
        this.loading = false;
        this.messageService.clear();
        //this.messageService.add({ key: 'tc', severity: 'success', summary: 'Correcto', detail: resp['message'] });
      }, error => {
        this.servidorService.getAllServers()
          .subscribe(dato_clientes => {
            this.loading = false;
            this.errorHandler(error, 1)
          });
      });
  }

  callFormUpdate(rowData: ServidorModel) {
    this.servidorService.getAllConnsByIdServer(rowData).subscribe(resp => {
      rowData.servers = resp;
      this.updateServidor = JSON.parse(JSON.stringify(rowData));
      this.reordenarConexiones(this.updateServidor);
      this.updateServidor.acciones = [];
      this.updateServidorO = JSON.parse(JSON.stringify(rowData));
      this.conexiones_disp.forEach(elem => {
        rowData.servers.forEach(server => {
          this.callDB(rowData.id, rowData.nombre);
          if (server.tipo_conexion === elem.value) {
            this.conexiones.push(elem.value);
            this.conexionesO.push(elem.value);
            if(server.tipo_conexion=="SSH"){
              if(server.tipoPassSsh=="key"){
                this.tipoPassSsh =
                  { label: 'Key', value: 'key' };
              }else {
                this.tipoPassSsh =
                  { label: 'Password', value: 'password' };
              }

            }
          }
        });
      });
      this.brands.forEach(elem => {
        if (elem.value == rowData.producto) {
          this.productoInsert = elem;
        }
      });
      this.display = true;
      this.insert = false;
      this.formulario = true;
    },
      error => this.errorHandler(error, 1));
  }


  //Acciones con el servidor

  //actualizar
  toUpdateServers() {
    this.loading = true;
    this.servidorService.updateServers(this.cambios).subscribe(
      resp => {
        this.servidorService.getAllServers()
          .subscribe(servers => {
            this.loading = false;
            this.servidorList = servers;
            this.updateTableButtonStatus = true;
            this.messageService.clear();
            this.messageService.add({ key: 'tc', severity: 'success', summary: 'Correcto', detail: resp['message'] });
          });
      }, error => {
        this.servidorService.getAllServers()
          .subscribe(servers => {
            this.loading = false;
            this.servidorList = servers;
            this.updateTableButtonStatus = true;
            this.errorHandler(error, 1)
          });
      });

    this.cambios = [];
  }

  //actualizar
  toUpdateConnsServer() {
    let datos = this.updateServidor;
    this.servidorService.updateConnsServer(datos).subscribe(resp => {
      this.close()
      this.servidorService.getAllServers()
        .subscribe(servers => {
          this.loading = false;
          this.servidorList = servers;
          this.messageService.clear();
          this.messageService.add({ key: 'tc', severity: 'success', summary: 'Correcto', detail: resp['message'] });
        }, error => this.errorHandler(error, 1));
      this.errorGral = false;
    }, error => this.errorHandler(error, 1));
  }

  //borrar
  toBorrarServers() {
    this.confirmationService.confirm({
      message: '¿Seguro que quiere borrar ' + this.selectedServidor.length + ' servidor(es) ?',
      header: 'Borrar servidores.',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.loading = true;
        let datos = [];

        for (let i = 0; i < this.selectedServidor.length; i++) {
          datos.push(this.selectedServidor[i]['id']);
        }

        this.servidorService.deletegeneral(datos).subscribe(
          resp => {
            this.loading = false;
            this.servidorService.getAllServers()
              .subscribe(servers => {
                this.loading = false;
                this.servidorList = servers;
                this.messageService.clear();
                this.messageService.add({ key: 'tc', severity: 'success', summary: 'Correcto', detail: 'Servidor eliminado correctamente' });
              });
          });
      }
    });
  }

  //borra un registro
  deleteIndividualRow(id) {
    let servidor = this.servidorList.filter(elem => elem.id == id);
    this.confirmationService.confirm({
      message: '¿Seguro que quiere borrar el servidor ' + servidor[0].nombre + '?',
      header: 'Borrar Servidor.',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.loading = true;
        this.servidorService.deleteServer(id).subscribe(resp => {
          this.servidorService.getAllServers()
            .subscribe(servers => {
              this.loading = false;
              this.servidorList = servers;
              this.messageService.clear();
              this.messageService.add({ key: 'tc', severity: 'success', summary: 'Correcto', detail: 'Servidor eliminado correctamente' });
            });
        });
      }
    });
  }

  //Almacena nuevos registros
  store() {
    this.limpiaInsertArray();
    let datos = this.insertServidor;
    this.servidorService.save(datos).subscribe(resp => {
      this.close()
      this.servidorService.getAllServers()
        .subscribe(servers => {
          this.loading = false;
          this.servidorList = servers;
          this.messageService.clear();
          this.messageService.add({ key: 'tc', severity: 'success', summary: 'Correcto', detail: resp['message'] });
        }, error => this.errorHandler(error, 1));
      this.errorGral = false;
    }, error => this.errorHandler(error, 1));
  }

  //almacena los cambios que se hagan
  onBlurMethod($campo, $id, $valor, fila = null) {
    if (($valor != null || $valor != '') && $valor.length > 3) {

      this.updateTableButtonStatus = false;
    } else {
      this.updateTableButtonStatus = true;
    }
    var validador = this.validaUpdateArray($id);
    if (validador != -1) {
      this.cambios[validador][$campo] = $valor;
    } else {
      this.cambios.push(fila);
    }
  }

  addConnInsert(valu = '', evento, conn, servidor: ServidorModel) {
    this.conexiones.sort();
    let conexion: ConexionesModel = new ConexionesModel;
    if (evento) {
      conexion.id = this.conexiones.indexOf(conn) + 1;
      conexion.tipo_conexion = conn;
      conexion.usuario = "";
      conexion.password = "";
      switch (conexion.tipo_conexion) {
        case "MySql":
          conexion.puerto = 3306;
          conexion.centraliza = false;
          break;
        case "PostgreSql":
          conexion.puerto = 5432;
          conexion.centraliza = false;
          break;
        case "SSH":
          conexion.puerto = 22;
          conexion.centraliza = false;
          break;
      }
      servidor.servers.push(conexion);
      this.reordenarConexiones(servidor);
    } else {
      servidor.servers = servidor.servers.filter(elem => {
        return elem.tipo_conexion != conn;
      });
    }
    if (this.insert) {
      this.validaInsertArray();
    }
  }

  addConnUpdate(check, conn) {
    this.conexiones.sort();
    if (check) {
      switch (this.conexionesO.includes(conn)) {
        case true:
          let reponer = this.updateServidorO.servers.find(elem => elem.tipo_conexion == conn);
          this.updateServidor.servers.push(reponer);
          this.updateServidor.acciones = this.updateServidor.acciones.filter(elem => elem.accion != "D" && elem.id_conn != reponer.id)
          this.updateDialogButtonStatus = !(this.updateServidor.acciones.length > 0);
          break;
        case false:
          let conexion: ConexionesModel = new ConexionesModel;
          conexion.id = this.conexiones.indexOf(conn) + 1;
          conexion.tipo_conexion = conn;
          conexion.usuario = "";
          conexion.password = "";
          switch (conexion.tipo_conexion) {
            case "MySql":
              conexion.puerto = 3306;
              conexion.centraliza = false;
              break;
            case "PostgreSql":
              conexion.puerto = 5432;
              conexion.centraliza = false;
              break;
            case "SSH":
              conexion.puerto = 22;
              conexion.centraliza = false;
              break;
          }
          this.updateServidor.servers.push(conexion);
          this.updateServidor.acciones.push({ accion: "C", id_conn: conn });
          break;
      }
    } else {
      if (this.conexionesO.includes(conn)) {
        let temp = this.updateServidor.servers.filter(elem => elem.tipo_conexion == conn);
        this.updateServidor.acciones.push({ accion: "D", id_conn: temp[0].id });
        this.updateDialogButtonStatus = !(this.updateServidor.acciones.length > 0);
      } else {
        this.updateServidor.acciones = this.updateServidor.acciones.filter(elem => elem.id_conn != conn);
      }
      this.updateServidor.servers = this.updateServidor.servers.filter(elem => {
        return elem.tipo_conexion != conn;
      });
    }
    this.reordenarConexiones(this.updateServidor);
  }



  close() {
    setTimeout(() => {
      this.servidorForm.reset();
    }, 0);
    this.insertServidor = new ServidorModel;
    this.insertServidor.servers = [];
    this.updateServidor = new ServidorModel;
    this.updateServidorO = new ServidorModel;
    this.productoInsert = "";
    this.conexiones = [];
    this.errorGral = false;
    this.display = false;
    this.insert = true;
    this.insertButtonStatus = false;
    this.updateDialogButtonStatus = true;
    this.cambios = [];
    this.tipoPassSsh =
      { label: 'Password', value: 'password' };
  }

  //
  private reordenarConexiones(servidor) {

    servidor.servers.sort(function (a, b) {
      let nameA = a.tipo_conexion.toUpperCase();
      let nameB = b.tipo_conexion.toUpperCase();
      if (nameA < nameB) {
        return -1;
      }
      if (nameA > nameB) {
        return 1;
      }
    })
  }

  //limpia el arreglo de filas vacias
  limpiaInsertArray() {
    this.insertServidor.servers = this.insertServidor.servers.filter(elem => {
      return elem.tipo_conexion !== '' || elem.puerto !== null || elem.usuario !== '' || elem.password !== '';
    });
  }

  //validaciones
  //valida si el insertServidor sea mayor a 0 y activa el boton de guardar
  validaInsertArray() {
    let valida = this.insertServidor.servers.filter(elem => {
      return elem.tipo_conexion !== '' || elem.puerto !== null || elem.usuario !== '' || elem.password !== '';
    });

    valida = valida.filter(elem => {
      return elem.tipo_conexion === '' || elem.puerto === null || elem.usuario.length < 4 || elem.password.length < 8;
    });

    this.insertButtonStatus = (valida.length > 0);

  }

  validaCentral(){
    let datos = this.insertServidor;
    this.servidorService.obtenerBD(datos).subscribe(resp => {
      this.dbList = resp;
      this.servidorService.getAllServers()
        .subscribe(servers => {
          this.servidorList = servers;
        }, error => this.errorHandler(error, 1));
      this.errorGral = false;
      this.validador();
    }, error => {
      this.errorHandler(error, 1);
      this.validador();
    });
  }

  validador(){
    let valida = this.insertServidor.servers.filter(elem => {
      return elem.centraliza === true || elem.dbcentral !== undefined || elem.dbcentral !== '' || elem.puerto !== null || elem.usuario !== '' || elem.password !== '';
    });

    valida = valida.filter(elem => {
      return elem.centraliza !== true || elem.dbcentral === undefined || elem.dbcentral === '' || elem.puerto === null || elem.usuario.length < 4 || elem.password.length < 8;
    });

    this.insertButtonStatus = (valida.length > 0);
  }

  //valida si ya hubo un cambio antes en el mismo registro
  private validaUpdateArray($id) {
    var validador = this.cambios.findIndex((elem) => elem.id === $id);

    return validador;
  }

  validaServidorUpdate() {
    let validador = (this.updateServidorO.nombre == this.updateServidor.nombre && this.updateServidorO.ip_publica == this.updateServidor.ip_publica && this.updateServidorO.ip_local == this.updateServidor.ip_local && this.updateServidorO.producto == this.updateServidor.producto);
    this.updateDialogButtonStatus = validador;
  }

  validaConnsServidorUpdate(dato, id_conn, conn) {
    if (dato !== undefined || dato !== "") {
      let actualizado = this.updateServidor.servers.find(elem => elem.id == id_conn);
      let original = this.updateServidorO.servers.find(elem => elem.id == id_conn);
      if (this.conexionesO.includes(conn)) {
        let validacion = (actualizado.puerto == original.puerto && actualizado.password == original.password && dato!="archivo" && actualizado.usuario == original.usuario && actualizado.dbcentral == original.dbcentral && actualizado.producto == original.producto);
        if (validacion) {
          this.updateServidor.acciones = this.updateServidor.acciones.filter(elem => elem.accion != "U" && elem.id_conn != conn);
        } else {
          this.updateServidor.acciones.push({ accion: "U", id_conn: conn });
        }
        this.updateDialogButtonStatus = validacion;
      } else {
        let valida = (actualizado.puerto === null || actualizado.puerto === 0 || actualizado.usuario.length < 4 || actualizado.password.length < 7);
        this.updateDialogButtonStatus = valida;
      }
    } else { this.updateDialogButtonStatus = true }
  }

  //valida nombre repetido en el insert y existentes
  validaNombre(nombre) {
    let repetido = (this.servidorList.filter(elem => elem.nombre === nombre)).length > 0;
    if (repetido) {
      this.messageService.clear();
      this.messageService.add({ key: 'tc', severity: 'error', summary: 'Error', detail: 'Ya existe un servidor con este nombre.' });
      this.updateDialogButtonStatus = true;
    }
  }

  //valida si ya existe un registro con la misma ip, tipo de coexion y si es centralizada
  validaCentraliza(row: ServidorModel, id_central, tipo) {
    let lista: ServidorModel[] = JSON.parse(JSON.stringify(this.servidorList));
    if (tipo == 1) {
      var exiteList = lista.findIndex(elem => elem.ip_publica === row.ip_publica && elem.producto === row.producto && elem.id !== row.id);
      if (exiteList > -1) {
        var existeCentral = lista[exiteList].servers.findIndex(elem => elem.tipo_conexion == 'mysql' && elem.centraliza == true && elem.centraliza == row.servers[id_central].centraliza);
        if (existeCentral > -1) {
          this.messageService.clear();
          this.messageService.add({ key: 'tc', severity: 'error', summary: 'Error', detail: 'Ya existe un registro centralizado con la misma IP y tipo de conexión.' });
          this.insertButtonStatus = true;
          return true;
        }
      }
      this.insertButtonStatus = false;
      return false;
    }
    // else {
    //   console.log(this.servidorList.filter(elem => elem.ip === row.ip && elem.tipo_conexion === row.tipo_conexion && elem.producto === row.producto && elem.centraliza == true && elem.centraliza == row.centraliza && elem.id !== row.id));
    //   var exiteList = (this.servidorList.filter(elem => elem.ip === row.ip && elem.tipo_conexion === row.tipo_conexion && elem.producto === row.producto && elem.centraliza == true && elem.centraliza == row.centraliza && elem.id !== row.id)).length > 0;
    //   if (exiteList) {
    //     this.messageService.clear();
    //     this.messageService.add({ key: 'tc', severity: 'error', summary: 'Error', detail: 'Ya existe un registro centralizado con la misma IP y tipo de conexión.' });
    //     this.updateButtonStatus = true;
    //     return true;
    //   } else {
    //     this.updateButtonStatus = false;
    //     return false;
    //   }
    // }
  }

  //valida el formato de la ip
  validaIp(IP) {
    const re = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
    return (IP === 'localhost' || re.test(String(IP)));

  }

  errorHandler(error, tipo, elem = null) {
    if (tipo === 1) {
      let detalles = error['error']['message'];
      if (error['error']['error'] !== undefined) {
        detalles = detalles + ". " + error['error']['error']
      }
      this.messageService.clear();
      this.messageService.add({ key: 'tc', severity: 'error', summary: 'Error', detail: detalles });
    } else if (tipo === 2) {
      elem.ejecutado = 2;
      elem.error = error.error.message;
      this.messageService.clear();
      this.messageService.add({ key: 'tc', severity: 'success', summary: 'Correcto', detail: 'Datos insertados correctamente.' });
    }

  }

  loadFile(e,server:ConexionesModel) {
    const reader: FileReader = new FileReader();
    const file = e.files[0];
    reader.readAsText(file);
    reader.onload = (event: Event) => {
      server.private_key = reader.result;
      this.cancelar = true;
    };
  }

  borrar(server:ConexionesModel){
    server.private_key = undefined;
    this.cancelar = false;
  }

}