import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GirhaAlertasComponent } from './girha-alertas.component';

const routes: Routes = [{ path: '', component: GirhaAlertasComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GirhaAlertasRoutingModule { }
