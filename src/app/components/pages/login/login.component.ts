import { Component, OnInit } from '@angular/core';
import {UsuarioModel} from '../../../models/usuario/usuario.model';
import {NgForm} from '@angular/forms';
import {MessageService, PrimeNGConfig} from 'primeng/api';
import {HttpClient} from '@angular/common/http';
import {AuthService} from '../../../services/auth.service';
import {Router} from '@angular/router';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [MessageService]
})
export class LoginComponent implements OnInit {

  usuario: UsuarioModel;
  cargando = false;
  uploadedFiles: any[] = [];
  cancelar = false;


  constructor(private messageService: MessageService, private authService: AuthService,
              private router: Router, private httpClient: HttpClient,
              private primengConfig: PrimeNGConfig) { }

  ngOnInit(): void {
    this.usuario = new UsuarioModel();
    this.primengConfig.ripple = true;
    let creado;
    if( sessionStorage.getItem('creado') === '1'){
      creado = of(sessionStorage.getItem('creado')).pipe(delay(700));
      creado.subscribe(x=>{
        this.messageService.clear();
        this.messageService.add({key: 'tc', severity: 'success', summary: 'Cuenta creada', detail: 'Tu cuenta a sido creada, por favor espera la validación de un administrador.'});
        sessionStorage.removeItem('creado');
      })
    } else if(sessionStorage.getItem('creado') === '2'){
      creado = of(sessionStorage.getItem('creado')).pipe(delay(700));
      creado.subscribe(x=>{
        this.messageService.clear();
        this.messageService.add({key: 'tc', severity: 'success', summary: 'Acceso recuperado', detail: 'Tu nueva llave a sido creada. Por favor revisa tu correo.'});
        sessionStorage.removeItem('creado');
      })
    }
  }

   iniciarSesion(form: NgForm): void{

    if (!form.invalid && this.usuario.private_key.length !== undefined && (this.usuario.private_key.length > 0)){
      this.cargando = true;
      this.authService.login(this.usuario)
        .subscribe( resp => {
          this.cargando = false;
          localStorage.setItem('user_id_auth', resp['id']);
          this.router.navigateByUrl('/home');
      }, err => {
          this.cargando = false;
          console.log(err);
          this.messageService.clear();
          err['error']['message'] = err['error']['message'] == undefined ? 'Error de conexión al servidor' : err['error']['message'];
          this.messageService.add({key: 'tc', severity: 'error', summary: 'Datos erróneos', detail: err['error']['message']+' ('+err['error']['type']+')'});
      } );
    }else{
      this.messageService.clear();
      this.messageService.add({key: 'tc', severity: 'warn', summary: 'Campos', detail: 'Debe llenar los campos obligatorios'});
    }
  }

  loadFile(e) {
    const reader: FileReader = new FileReader();
    const file = e.files[0];
    reader.readAsText(file);
    reader.onload = (event: Event) => {
      this.usuario.private_key = reader.result;
      this.cancelar = true;
    };
  }

  focoLogin(){
    document.getElementById('button_login').focus();
  }

  borrar(){
    this.usuario.private_key = undefined;
    this.cancelar = false
  }
}
