import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import {CheckboxModule} from 'primeng/checkbox';
import {FormsModule} from '@angular/forms';
import {InputTextModule} from 'primeng/inputtext';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {ButtonModule} from 'primeng/button';
import {RippleModule} from 'primeng/ripple';
import {FileUploadModule} from 'primeng/fileupload';
import {ToastModule} from 'primeng/toast';
import {HttpClientModule} from '@angular/common/http';
import {PasswordModule} from 'primeng/password';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import {LoadingModule} from '../../shared/loading/loading.module';
import {LoginComponent} from './login.component';



@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    LoginRoutingModule,
    CheckboxModule,
    FormsModule,
    InputTextModule,
    InputTextareaModule,
    ButtonModule,
    RippleModule,
    FileUploadModule,
    ToastModule,
    HttpClientModule,
    PasswordModule,
    ProgressSpinnerModule,
    LoadingModule,
  ]
})
export class LoginModule { }
