import { Component, HostListener, OnInit } from '@angular/core';
import {MessageService} from 'primeng/api';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {ServidorService} from '../../../../services/servidor.service';
import {LectoresCronService} from '../../../../services/lectores-cron.service';
import {LectorCronHorasModel, LectorCronModel} from 'src/app/models/lector/lector.model';
import {ServidorModel} from '../../../../models/servidor/servidor.model';

@Component({
  selector: 'app-lectores-configuracion',
  templateUrl: './lectores-configuracion.component.html',
  styleUrls: ['./lectores-configuracion.component.css'],
  providers: [MessageService]
})
export class LectoresConfiguracionComponent implements OnInit {
  lectorCronList: LectorCronModel[];
  lectorCron: LectorCronModel;
  insertLectorCron: LectorCronModel;
  insertLectorCronHoras: LectorCronHorasModel[];
  updateLectorCron: LectorCronModel;
  updateLectorCronO: LectorCronModel;
  brands: any[];
  tipos_horas: any[];
  estadoInsert = undefined;

  cambios = [];
  collection = [];
  cont = 0;
  search = "";
  cols: any[];
  colsInsert: any[];
  selectedColumns: any[];
  selectedAlerta: any;
  formulario: true;
  arg = 1;
  display = true;
  loading = false;
  errorGral = false;
  mostrar = false;
  insert = true;
  msj = '';

  submitted: boolean;
  lectorCronForm: FormGroup;
  lectorCronInsertform: FormGroup;

  lectorCronInsert = undefined;

  // Validacion y bloqueos
  updateButtonStatus: boolean = true;
  deleteButtonStatus: boolean = true;
  insertButtonStatus: boolean = true;
  deleteInsertButtonStatus: boolean = true;
  cerrarInsertButtonStatus: boolean = false;
  validacionIDCLAVE: boolean = false;

  updateDialogButtonStatus: boolean = true;


  userCanRead: any;
  userCanEdit: any;
  userCanDelete: any;
  userCanLoad: any;
  userCanInsert: any;

  fileToUpload: any;
  currentRoute: any;
  userPermissions: any[];

  constructor(private fb: FormBuilder, private messageService: MessageService, private router: Router,
              private servidorService: ServidorService, private lectoresCronService: LectoresCronService) {
    this.loading = true;
    this.currentRoute = this.router.url;
    this.lectoresCronService.getAllLecCrons()
      .subscribe(resp => {
        this.loading = false;
        this.lectorCronList = resp;
        if (this.lectorCronList.length > 0) {
          this.mostrar = true;
        }
      });

    this.brands = [
      { label: 'Activo', value: 1 },
      { label: 'Desactivado', value: 0 }];

    this.tipos_horas = [
      { label: 'Por lapso', value: 'lapso' },
      { label: 'Por hora', value: 'hora' }];

  }

  ngOnInit(): void {

    this.userCanRead = 1;
    this.userCanEdit = 1;
    this.userCanDelete = 1;
    this.userCanLoad = 1;
    this.userCanInsert = 1;

    this.lectorCronForm = this.fb.group({
      'nombre': new FormControl('', Validators.required),
      'estado': new FormControl('', Validators.required),
      'hora': new FormControl('', Validators.required),
      'tipo_hora': new FormControl('', Validators.required),
    });

    this.cols = [
      { field: 'nombre', header: 'Nombre', width: '150px' },
      { field: 'estado', header: 'Estado', width: '150px' },
/*      { field: 'hora', header: 'Hora', width: '150px' },
      { field: 'tipo_hora', header: 'Tipo hora', width: '150px' },*/
    ];

    this.selectedColumns = this.cols;
    this.lectorCronList = [];
  }

  buscarDatos() { }

  callFormInsert() {
    this.lectorCronInsertform = this.fb.group({
      'nombre': new FormControl('', Validators.required),
      'estado': new FormControl('', Validators.required),
/*      'hora': new FormControl('', Validators.required),
      'tipo_hora': new FormControl('', Validators.required),*/
    });

    this.colsInsert = [
      { field: 'id', header: 'Id', width: '50px' },
      { field: 'hora', header: 'Hora', width: '150px' },
    ];

    this.insertLectorCron =
      { id: 1, nombre: '', estado: 2, tipo_hora: '' };
    this.insertLectorCronHoras =[];

    this.display = true;
    this.formulario = true;
    this.insert = true

  }

  toBorrar() {
    this.loading = true;
    let datos = [];

    for (let i = 0; i < this.selectedAlerta.length; i++) {
      datos.push(this.selectedAlerta[i]['nombre']);
    }

    this.servidorService.deletegeneral(datos).subscribe(
      resp => {

        this.loading = false;
        this.servidorService.getAllServers()
          .subscribe(servers => {
            this.loading = false;
            this.lectorCronList = servers;

            this.messageService.clear();
            this.messageService.add({ key: 'tc', severity: 'success', summary: 'Correcto', detail: 'Servidor eliminado correctamente' });
          });
      });
  }


  toUpdate() {


    this.lectoresCronService.update(this.cambios).subscribe(
      resp => {
        this.lectoresCronService.getAllLecCrons()
          .subscribe(servers => {
            this.loading = false;
            this.lectorCronList = servers;
            this.updateButtonStatus = true;
            this.messageService.clear();
            this.messageService.add({ key: 'tc', severity: 'success', summary: 'Correcto', detail: 'Alerta(s) actualizada(s) correctamente' });
          });
      },
    );
    this.cambios = [];

  }

  makeButtonVisible(event) {
    let total = this.lectorCronList.length;
    let posicion = event.index;
    let activar = this.selectedAlerta.length;
    if (activar === 1) {
      this.deleteButtonStatus = true;
      for (let i = 0; i < total; i++) {
        if (posicion === i) {
          this.lectorCronList[posicion]['activo'] = 0;
        } else {
          this.lectorCronList[i]['activo'] = 1;
        }
      }
    } else if (activar < 1) {
      this.deleteButtonStatus = false;
    } else {
      this.deleteButtonStatus = false;
      for (let i = 0; i < total; i++) {
        this.lectorCronList[i]['activo'] = 1;
      }
    }
  }

  activeUpdateButton(data) {
    if (data != null || data != '') {

      this.updateButtonStatus = false;
    } else {
      this.updateButtonStatus = true;
    }
  }

  onBlurMethod($campo, $id, $valor, fila = null) {

    this.activeUpdateButton($valor)
    var validador = this.validaUpdateArray($id);
    if (validador != -1) {
      this.cambios[validador][$campo] = $valor;
    } else {
      this.cambios.push(fila);
    }
  }


  deleteIndividualRow(id) {
    this.loading = true;
    this.servidorService.deleteServer(id).subscribe(resp => {
      this.servidorService.getAllServers()
        .subscribe(servers => {
          this.loading = false;
          this.lectorCronList = servers;

          this.messageService.clear();
          this.messageService.add({ key: 'tc', severity: 'success', summary: 'Correcto', detail: 'Servidor eliminado correctamente' });
        });
    });
  }

  close() {
    this.estadoInsert = "";
    setTimeout(() => {
      this.lectorCronInsert.reset();
      this.lectorCronForm.reset();
    }, 0);
    this.errorGral = false;
    this.display = false;
    this.insertButtonStatus=true;
    this.arg=1;

  }

  addRow() {

      this.insertLectorCronHoras.push({ id: this.arg, hora: "", id_table: null, sentencia:"" },);
      this.insertButtonStatus=true;


    this.arg++;
  }

  store() {
    this.limpiaInsertArray();
    let datos = this.insertLectorCron;
    datos.horas=this.insertLectorCronHoras;


    this.lectoresCronService.save(datos).subscribe(resp => {
      this.insertLectorCron =
        { id: 1, nombre: '', estado: 2, tipo_hora: '' };
      this.insertButtonStatus = true;
      this.lectoresCronService.getAllLecCrons()
        .subscribe(alerts => {
          this.loading = false;
          this.lectorCronList = alerts;
          this.display = false;
        if (this.lectorCronList.length > 0) {
          this.mostrar = true;
        }
          this.messageService.clear();
          this.messageService.add({ key: 'tc', severity: 'success', summary: 'Correcto', detail: 'Alerta(s) insertada(s) correctamente' });
        }, error => this.errorHandler(error, 1));
      this.errorGral = false;
    },error => this.errorHandler(error, 1));

  }

  removeRow(id) {
    this.arg--;
    for (let i = 0; i < this.insertLectorCronHoras.length; i++) {
      if (this.insertLectorCronHoras[i]['id'] == id) {
        let id_table = this.insertLectorCronHoras[i]['id_table'];
        this.insertLectorCronHoras.splice(i, 1);

          if(!this.insert){
              if(id_table!=null){
              if(this.insertLectorCronHoras.length>0){
                this.insertButtonStatus=false;
              }else{
                this.insertButtonStatus=true;
              }
            }
              else {
                let notnull =this.insertLectorCronHoras.filter(elem =>{
                  return  elem.id_table!=null;
                });
                let nulo =this.insertLectorCronHoras.filter(elem =>{
                  return  elem.id_table==null;
                });
                if(notnull.length>0 && !(nulo.length>0)){
                  this.insertButtonStatus=false;
                }else{
                  this.insertButtonStatus=true;
                }

              }

        }else {
            if(!(this.insertLectorCronHoras.length>0)){
              this.insertButtonStatus=true;
            }
          }
      }
    }

    for (let j = 0; j < this.insertLectorCronHoras.length; j++) {
      this.insertLectorCronHoras[j]['id'] = j + 1;
    }
  }

  limpiaInsertArray() {
    this.insertLectorCronHoras = this.insertLectorCronHoras.filter(elem => {
      return elem.hora !== '' || elem.sentencia !=='' || elem.id_table !== null;
    });
  }


  validaInsertArray() {
    let valida =
      (this.insertLectorCron.nombre !== '' || this.insertLectorCron.estado != null || this.insertLectorCron.tipo_hora !== ''||this.insertLectorCron.horas.length >0);
    ;

    this.insertButtonStatus = !valida;

  }
  validaLectorCronUpdate() {
    let validador = (this.updateLectorCronO.nombre == this.updateLectorCron.nombre && this.updateLectorCronO.estado == this.updateLectorCron.estado );
    this.insertButtonStatus=validador
    this.updateDialogButtonStatus = validador;

  }

  validaNombre(nombre) {
   /* let repetidoInsert = (this.insertLectorCron.filter(elem => elem.nombre === nombre && elem.nombre !== '')).length > 1;
    if ((repetidoInsert) || ((this.lectorCronList.filter(elem => elem.nombre === nombre)).length > 0)) {
      this.msj = 'Nombre repetido';
      return true;
    } else {
      this.msj = '';
      return false;
    }*/
    return false;
  }

  private validaUpdateArray($id) {
    var validador = this.cambios.findIndex((elem) => elem.id === $id);
    return validador;
  }

  errorHandler(error, tipo, elem = null) {
    if (tipo === 1) {
      console.log(error);
      this.messageService.clear();
      this.messageService.add({ key: 'tc', severity: 'error', summary: 'Error', detail: error['error']['message'] });
    } else if (tipo === 2) {
      console.log(error);
      elem.ejecutado = 2;
      elem.error = error.error.message;
      this.messageService.clear();
      this.messageService.add({ key: 'tc', severity: 'success', summary: 'Correcto', detail: 'Datos insertados correctamente' });
    }

  }

  callFormUpdate(rowData: LectorCronModel) {
    console.log(rowData);
    this.lectoresCronService.getAllHorasCron(rowData).subscribe(resp => {
        rowData.horas = resp;
        this.updateLectorCron = JSON.parse(JSON.stringify(rowData));
        console.log(rowData.horas)
        /*this.reordenarConexiones(this.updateLectorCron);*/
        this.updateLectorCron.acciones = [];
        this.updateLectorCronO = JSON.parse(JSON.stringify(rowData));
        console.log(rowData);
        this.lectorCronInsertform = this.fb.group({
          'nombre': new FormControl('', Validators.required),
          'estado': new FormControl('', Validators.required),
          /*      'hora': new FormControl('', Validators.required),
                'tipo_hora': new FormControl('', Validators.required),*/
        });
        let iter=1;
        this.insertLectorCronHoras=[];
        rowData.horas.forEach(server => {
          this.insertLectorCronHoras.push({ id: iter, hora: server.hora, id_table: server.id, sentencia: server.sentencia},)
          iter=iter+1;
        });
        this.arg=iter;
        this.brands.forEach(elem => {
          if (elem.value == rowData.estado) {
            this.estadoInsert = elem;
          }
        });
        this.updateLectorCron = JSON.parse(JSON.stringify(rowData));
        this.display = true;
        this.insert = false;
        this.formulario = true;
      },
      error => this.errorHandler(error, 1));
  }


  //actualizar
  toUpdateCronHoras() {

    this.updateLectorCron.horas=this.insertLectorCronHoras;
    console.log(this.updateLectorCron);
    let datos = this.updateLectorCron;
    this.lectoresCronService.updateCronHoras(datos).subscribe(resp => {
      this.close();
      this.lectoresCronService.getAllLecCrons()
        .subscribe(servers => {
          this.loading = false;
          this.lectorCronList = servers;
          if (this.lectorCronList.length > 0) {
            this.mostrar = true;
          }
          this.messageService.clear();
          this.messageService.add({ key: 'tc', severity: 'success', summary: 'Correcto', detail: resp['message'] });
        }, error => this.errorHandler(error, 1));
      this.errorGral = false;
    }, error => this.errorHandler(error, 1));
  }

}
