import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LectoresConfiguracionComponent } from './lectores-configuracion.component';

describe('LectoresConfiguracionComponent', () => {
  let component: LectoresConfiguracionComponent;
  let fixture: ComponentFixture<LectoresConfiguracionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LectoresConfiguracionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LectoresConfiguracionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
