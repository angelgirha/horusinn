import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService, PrimeNGConfig } from 'primeng/api';
import { UsuarioModel } from 'src/app/models/usuario/usuario.model';
import { AuthService } from 'src/app/services/auth.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-reset-key-form',
  templateUrl: './reset-key-form.component.html',
  styleUrls: ['./reset-key-form.component.css'],
  providers: [MessageService]
})
export class ResetKeyFormComponent implements OnInit {

  usuario: UsuarioModel;
  token: string;
  cargando = false;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private primengConfig: PrimeNGConfig,
              private messageService: MessageService,private authService: AuthService) { }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(
      params => {
        this.token = params['token'];
      }
    );
    this.usuario = new UsuarioModel();
    this.primengConfig.ripple = true;
  }

  enviar(form: NgForm):void{
    if (!form.invalid && form.controls.password.touched && form.controls.password.dirty){
      this.cargando = true;
      this.authService.resetKey(this.usuario,this.token)
        .subscribe( resp => {
          this.cargando = false;
          sessionStorage.setItem('creado','2');
          this.router.navigate(['login']);
        }, err => {
          this.cargando = false;
          this.messageService.clear();
          err['ok'] == false ? this.messageService.add({key: 'tc', severity: 'warn', summary: 'En proceso', detail: 'Acción realizada previamente'+' (g-404)'}) : this.messageService.add({key: 'tc', severity: 'error', summary: 'Datos erroneos', detail: err['error']['message']+' ('+err['error']['type']+')'});
        } );
    }else{
      if (form.controls.password.touched || form.controls.password.dirty){
        this.messageService.clear();
        this.messageService.add({key: 'tc', severity: 'error', summary: 'Campos', detail: 'El correo fue modificado, no es posible realizar ninguna acción.'});
      }else{
        this.messageService.clear();
        this.messageService.add({key: 'tc', severity: 'warn', summary: 'Campos', detail: 'Debe llenar los campos obligatorios.'});
      }
    }
  }
}
