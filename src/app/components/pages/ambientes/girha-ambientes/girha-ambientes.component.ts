import { Component, HostListener, OnInit } from '@angular/core';
import { AmbienteModel } from '../../../../models/ambiente/ambiente.model';
import { LogModel } from '../../../../models/log/log.model';
import { AmbientesService } from '../../../../services/ambientes.service';
import { LogService } from '../../../../services/log.service';
import { NgForm } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { Router } from '@angular/router';
import { isNull } from '@angular/compiler/src/output/output_ast';
import { isNumber } from '@ng-bootstrap/ng-bootstrap/util/util';
import { resolve } from 'dns';

@Component({
  selector: 'app-girha-ambientes',
  templateUrl: './girha-ambientes.component.html',
  styleUrls: ['./girha-ambientes.component.css'],
  providers: [MessageService]
})



export class GirhaAmbientesComponent implements OnInit {

  @HostListener('window:beforeunload', ['$event'])
  beforeUnloadHander(event) {
    if (this.verificado && !this.completado && !this.correo || !this.verificado && this.completado && !this.correo || this.verificado && this.completado && !this.correo) {
      console.log(event);
      // failed attempts to prevent popup =>
      event.preventDefault();
      event.returnValue = false;
      event.stopPropagation();
    }
  }

  ambiente: AmbienteModel;
  log_ambiente: LogModel;
  serversApli: any;
  tipoContac = [
    { name: "Funcional", code: "funcionalUser" },
    { name: "Técnico", code: "tecnicoUser" },
    { name: "Comercial", code: "adminUser" },
  ];
  verificar = false;
  verificarCliente = false;
  userRFC = '';
  cliente = '';
  serversEngine: any;
  tables: any;
  funciones: any;
  events: any;
  sp: any;
  triggers: any;
  views: any;
  tareas: any;
  extras: any;
  verificado = false;
  progressPorcent = 0;
  total: number;
  numEjecutados = 0;
  msgs1 = [];
  detail = [];
  correos:string[] = [];
  detalles: any;
  empresaError = false;
  empresaMysql = false;
  empresaPostgresql = false;
  cargando = false;
  completado = false;
  correo = false;
  enviado = false;
  error = false;
  mostrar = false;
  limpiar = false;
  bloqueo = false;

  constructor(private ambientesService: AmbientesService, private logService: LogService, private router: Router,) { }


  ngOnInit(): void {
    this.ambiente = new AmbienteModel();
    this.log_ambiente = new LogModel();
    this.ambiente.producto = 'girha';
    this.getServidores();
    this.logService.obtenerUsuario();
  }

  //funcion principal
  async nuevo(form: NgForm, tipo) {
    if (!form.invalid && !this.empresaError && this.ambiente.cliente !== undefined) {
      if (tipo === 1) {
        this.msgs1 = [];
        this.detail = [];
        this.error = false;
        this.cargando = true;
        //obtiene todos los elementos para la BD del cliente
        this.ambientesService.getTablesName(this.ambiente).subscribe(
          resp => {
            this.tables = resp;
            this.ambientesService.getFunciones(this.ambiente).subscribe(
              funciones => {
                this.funciones = funciones;
                this.ambientesService.getEvents(this.ambiente).subscribe(
                  events => {
                    this.events = events;
                    this.ambientesService.getSP(this.ambiente).subscribe(
                      sp => {
                        this.sp = sp;
                        this.ambientesService.getTriggers(this.ambiente).subscribe(
                          triggers => {
                            this.triggers = triggers;
                            this.ambientesService.getViews(this.ambiente).subscribe(
                              views => {
                                this.views = views;
                                this.verificado = true;
                                this.cargando = false;
                                this.total = this.tables.length + this.funciones.length + this.events.length +
                                  + this.sp.length + this.triggers.length + this.views.length;
                                this.igualaArray();
                                this.recuperarDBModelo();
                              }, error => this.errorHandler(error, 1));
                          }, error => this.errorHandler(error, 1));
                      }, error => this.errorHandler(error, 1));
                  }, error => this.errorHandler(error, 1));
              }, error => this.errorHandler(error, 1));
          }, error => this.errorHandler(error, 1));
        this.crearLog();
      }
      else if (tipo === 2) {
        this.recuperarDBModelo();
      } else if (tipo === 3) {
        this.ejecutaTareasAdicionales();
      } else if (tipo === 4 && this.ambiente.email != null) {
        this.ultimoPaso();
      } else {
        this.msgs1 = [{ severity: 'error', summary: '¡Datos incompletos!', detail: '¡Favor de verificar los campos!' }];
      }
    } else {
      this.msgs1 = [{ severity: 'error', summary: '¡Datos Erroneos!', detail: '¡Favor de verificar los campos!' }];
    }
  }

  async recuperarDBModelo(){
    this.bloqueo = true;
    if (this.ambiente.bd_nueva === undefined) {
      await new Promise((resolve, reject) => {
        //solicita crear la BD para el cliente
        this.log_ambiente.campo = 'base';
        this.actualizarEstadoLog(0);
        this.ambiente.name = 'base de datos';
        this.ambientesService.creaBase(this.ambiente).subscribe(
          async base => {
            this.actualizarEstadoLog(1);
            this.ambiente.bd_nueva = base['name'];
            resolve();
          }, error => this.errorHandler(error, 2, null, this.ambiente.name));
      });
    }
    this.error = false;
    this.msgs1 = [];
    this.detail = [];
    this.log_ambiente.campo = 'tablas';
    this.actualizarEstadoLog(0);
    for (const elem of this.tables) {
      if (elem.ejecutado !== -1 && elem.ejecutado !== 1) {
        this.ambiente.name = elem.Tables_name;
        await new Promise((resolve, reject) => {
          //solicita crear las tablas y las vistas las crea con info temporal
          this.ambientesService.creaTablas(this.ambiente)
            .subscribe(resp => {
              elem.ejecutado = 1;
              this.actualizaProgress();
              resolve();
              /* setTimeout(() => { resolve(); }, 500);*/
            }, error => this.errorHandler(error, 2, elem, this.ambiente.name));
        });
      }
    }
    this.actualizarEstadoLog(1);
    this.log_ambiente.campo = 'funciones';
    this.actualizarEstadoLog(0);
    for (const elem of this.funciones) {
      if (elem.ejecutado !== -1 && elem.ejecutado !== 1) {
        this.ambiente.name = elem.Name;
        await new Promise((resolve, reject) => {
          //solicita crear las funciones
          this.ambientesService.creaFunciones(this.ambiente)
            .subscribe(resp => {
              elem.ejecutado = 1;
              this.actualizaProgress();
              resolve();
              /* setTimeout(() => { resolve(); }, 500);*/
            }, error => this.errorHandler(error, 2, elem, this.ambiente.name));
        });
      }
    }
    this.actualizarEstadoLog(1);
    this.log_ambiente.campo = 'eventos';
    this.actualizarEstadoLog(0);
    for (const elem of this.events) {
      if (elem.ejecutado !== -1 && elem.ejecutado !== 1) {
        this.ambiente.name = elem.Name;
        await new Promise((resolve, reject) => {
          //solicita crear los eventos
          this.ambientesService.creaEvents(this.ambiente)
            .subscribe(resp => {
              elem.ejecutado = 1;
              this.actualizaProgress();
              resolve();
            }, error => this.errorHandler(error, 2, elem, this.ambiente.name));
        });
      }
    }
    this.actualizarEstadoLog(1);
    this.log_ambiente.campo = 'sp';
    this.actualizarEstadoLog(0);
    for (const elem of this.sp) {
      if (elem.ejecutado !== -1 && elem.ejecutado !== 1) {
        this.ambiente.name = elem.Name;
        await new Promise((resolve, reject) => {
          //solicita crear los SP
          this.ambientesService.creaSP(this.ambiente)
            .subscribe(resp => {
              elem.ejecutado = 1;
              this.actualizaProgress();
              resolve();
            }, error => this.errorHandler(error, 2, elem, this.ambiente.name));
        });
      }
    }
    this.actualizarEstadoLog(1);
    this.log_ambiente.campo = 'triggers';
    this.actualizarEstadoLog(0);
    for (const elem of this.triggers) {
      if (elem.ejecutado !== -1 && elem.ejecutado !== 1) {
        this.ambiente.name = elem.Trigger;
        await new Promise((resolve, reject) => {
          //solicita crear los triggers
          this.ambientesService.creaTriggers(this.ambiente)
            .subscribe(resp => {
              elem.ejecutado = 1;
              this.actualizaProgress();
              resolve();
            }, error => this.errorHandler(error, 2, elem, this.ambiente.name));
        });
      }
    }
    this.actualizarEstadoLog(1);
    this.log_ambiente.campo = 'views';
    this.actualizarEstadoLog(0);
    for (const elem of this.views) {
      if (elem.ejecutado !== -1 && elem.ejecutado !== 1) {
        this.ambiente.name = elem.Tables_name;
        await new Promise((resolve, reject) => {
          //solicita crear las vistas
          this.ambientesService.creaViews(this.ambiente)
            .subscribe(resp => {
              elem.ejecutado = 1;
              this.actualizaProgress();
              resolve();
            }, error => this.errorHandler(error, 2, elem, this.ambiente.name));
        });
      }
    }
    this.actualizarEstadoLog(1);
    this.completado = true;
    this.verificado = false;
    this.bloqueo = false;
    this.msgs1 = [{ severity: 'success', summary: '¡Completado!', detail: 'La base de datos del ambiente se ha creado' }];
    this.tareasAdicionales();
  }

  async tareasAdicionales(){
    this.cargando = true;
    //obtiene tareas adicionales que se requieren
    this.ambientesService.getTareasAdicionales(this.ambiente).subscribe(
      tareas => {
        this.tareas = tareas;
        this.verificado = true;
        this.cargando = false;
        this.total = this.tareas.length;
        this.numEjecutados = 0;
        this.progressPorcent = 0;
        this.msgs1 = [];
        this.detail = [];
        this.ejecutaTareasAdicionales();
      }, error => this.errorHandler(error, 2, null, 'Obtener tareas adicionales'));
  }

  async ejecutaTareasAdicionales(){
    this.bloqueo = true;
    this.log_ambiente.campo = 'tareas';
    this.actualizarEstadoLog(0);
    this.error = false;
    this.msgs1 = [];
    this.detail = [];
    for (const elem of this.tareas) {

      if (elem.ejecutado !== -1 && elem.ejecutado !== 1) {
        this.ambiente.name = elem.tabla;
        await new Promise((resolve, reject) => {
          //define informacion default
          this.ambientesService.creaInformacionDefault(this.ambiente)
            .subscribe(resp => {
              elem.ejecutado = 1;
              this.actualizaProgress();
              resolve();
            }, error => this.errorHandler(error, 2, elem, this.ambiente.name));
        });
      }
    }
    this.cargando = true;
    this.correo = true;
    await new Promise((resolve, reject) => {
      //obtiene ultimas acciones
      this.ambientesService.getUltimosPasos(this.ambiente).subscribe(
        resp => {
          console.log(resp)
          this.cargando = false;
          this.extras = resp;
          this.enviado = false;
          this.total = this.extras.length;
          this.numEjecutados = 0;
          this.progressPorcent = 0;
          this.bloqueo = false;
          this.msgs1 = [{ severity: 'success', summary: '¡Casi completo!', detail: 'Escriba el email del cliente' }];

          setTimeout(() => {
            this.ultimoPaso();
          }, 60000);

          resolve();
        }, error => this.errorHandler(error, 2, this.ambiente.name)
      );
    });

    this.actualizarEstadoLog(1);
  }

  async ultimoPaso(){
    this.bloqueo = true;
    this.error = false;
    this.msgs1 = [];
    this.detail = [];
    this.log_ambiente.campo = 'extras';
    this.actualizarEstadoLog(0);
    await new Promise((resolve, reject) => {
      //inserta al nuevo cliente
      this.ambientesService.insetNewCliente(this.ambiente)
        .subscribe(resp => {
          resolve();
        }, error => this.errorHandler(error, 2, null, this.ambiente.name));
    });
    for (const elem of this.extras) {
      this.ambiente.name = elem.Name
      if (elem.ejecutado !== -1 && elem.ejecutado !== 1) {
        if (elem.Name != 'enviar_correo') {
          await new Promise((resolve, reject) => {
            this.ambientesService.execProcedure(this.ambiente).subscribe(
              //ejecuta el SP sp_actualiza_ip_clientes
              resp => {
                elem.ejecutado = 1;
                this.actualizaProgress();
                resolve();
              }, error => this.errorHandler(error, 2, elem, this.ambiente.name)
            );
          });
        } else {
          await new Promise((resolve, reject) => {
            //solicitud de correo
            this.ambientesService.envioEmail(this.ambiente).subscribe(
              resp => {
                elem.ejecutado = 1;
                this.enviado = true;
                this.bloqueo = false;
                this.msgs1 = [{ severity: 'success', summary: '¡Completado!', detail: '¡El ambiente esta listo!' }];
                this.actualizaProgress();
                resolve();
              }, error => this.errorHandler(error, 2, elem, this.ambiente.name)
            );
          });
        }
      }
    }
    this.actualizarEstadoLog(1);
    this.reiniciarValores();
  }

  //funciones de control interno del front
  //reinicia valores
  reiniciarValores() {
    window.location.reload();
  }

  //funcion para generar de password
  generarPass() {
    this.ambiente.password = Math.random().toString(36).substr(2, 10);
  }

  //funcion para generar de password
  generarUser(razon){
    this.msgs1 = [];
    if(this.ambiente.rfc !== undefined && this.ambiente.descEmpresa !== undefined && this.ambiente.dirEmpresa !== undefined){
      if(this.ambiente.tipoContacto1 !==undefined && this.ambiente.email_contacto1 !== undefined &&  this.ambiente.telefono1 !== undefined){
        if(isNaN(razon.substr(3,1))){
          this.userRFC = razon.replace(/ /g, "").toLowerCase().substr(0,4).toUpperCase();
        }else{
          this.userRFC = razon.replace(/ /g, "").toLowerCase().substr(0,3).toUpperCase();
        }
        this.ambiente.usuario = this.userRFC;
        this.generarPass();
        this.verificar = true;
        this.ambiente.cliente = this.cliente;
        this.msgs1 = [];
      }else{
        this.msgs1 = [{ severity: 'warn', summary: 'Alerta', detail: 'Debe capturar al menos un contacto' }];
      }
    }else{
      this.msgs1 = [{ severity: 'warn', summary: 'Alerta', detail: 'Debe capturar todos los datos del cliente' }];
    }
  }

  async validaLectores(){
    this.completado = false;
    this.verificado = false;
    this.msgs1 = [];
    this.cargando = true;

    if (this.ambiente.server_name !== null && this.ambiente.server_2 !== null && this.ambiente.empleadosActivos !== null && this.ambiente.lectoresLinea !== null) {
      await new Promise((resolve, reject) => {
        this.ambientesService.validarEmpleadosLectores(this.ambiente)
          .subscribe(resp => {
            console.log(resp);
            this.empresaError = false;
            this.detalles = undefined;
            this.verificarCliente = true;
            this.cargando = false;
            resolve();
          }, error => {
            console.log(error);
            this.msgs1 = [{ severity: 'error', summary: '¡Error!', detail: error.error.message }];
            this.cargando = false;
          });
      });
    }
  }

  //funcion para determinar el total para la funcion de la barra de progreso
  igualaArray() {
    let numero = Math.max(this.tables.length, this.funciones.length, this.events.length,
      this.sp.length, this.triggers.length, this.views.length);
    let todos = [];
    todos.push(this.tables, this.funciones, this.events, this.sp, this.triggers, this.views);
    for (const elem of todos) {
      if (elem.length > 0 && elem.length < numero) {
        while (elem.length < numero) {
          elem.push({ ejecutado: -1 });
        }
      }
    }
  }

  agregarCorreo() {
    this.msgs1 = [];
    let fallidos = 0;
    this.correos = this.correos.filter(elem=>{
      if(this.validateEmail(elem)){
        return true;
      }else{
        fallidos ++;
      }
    });
    this.listaCorreos();
    console.log(this.correos);
    console.log(this.ambiente.email);
    if(fallidos > 0){
      this.msgs1 = [{ severity: 'error', summary: '¡Error!', detail: 'Ingrese un email valido, por favor verificar y reintentar.' }];
    }
  }

  listaCorreos() {
    if(this.ambiente.email_contacto1 !== undefined && this.ambiente.email_contacto1 !== ''){
      if(this.correos.length > 0){
        this.ambiente.email = this.ambiente.email_contacto1+';'+ this.correos.join(';');
      }else{
        this.ambiente.email = this.ambiente.email_contacto1;
      }
    }
    console.log(this.ambiente.email);
    console.log(this.correos);
  }

  //funcion para mostrar el avance en la barra de progreso
  actualizaProgress() {
    this.numEjecutados++;
    this.progressPorcent = +((this.numEjecutados / this.total) * 100).toFixed(2);
  }

  //funciones para obtener los servers aplicativos y engine
  async getServidores() {
    await new Promise((resolve, reject) => {
      this.ambientesService.getServerbyProduct(this.ambiente)
        .subscribe(resp => {
          this.serversApli = resp.filter(elem => elem['tipo_conexion'] == 'MySql');
          this.serversEngine = resp.filter(elem => elem['tipo_conexion'] == 'PostgreSql');
          if (this.serversApli.length > 0 && this.serversEngine.length > 0) {
            this.mostrar = true;
          }
          resolve();
        }, error => this.errorHandler(error, 1));
    });

  }

  //funcion para actualizar el numero de cliente
  async acuatizaCliente(nombre) {
    this.ambiente.server_name = nombre;
    this.ambiente.cliente = '';
    if (nombre !== null) {
      await new Promise((resolve, reject) => {
        this.ambientesService.getLastCliente(this.ambiente)
          .subscribe(resp => {
            this.cliente = resp[0].total + 1;
            resolve();
          }, error => this.errorHandler(error, 1));
      });
    }
  }

  //datos para log
  private crearLog() {
    this.log_ambiente.user_id = sessionStorage.getItem('id_user');
    this.log_ambiente.producto = this.ambiente.producto;
    this.log_ambiente.cliente = this.ambiente.cliente;
    this.logService.crearLog(this.log_ambiente).subscribe(
      resp => {
        this.log_ambiente.log_id = resp['id_log'];
      },
      error => this.errorHandler(error, 2, null, 'Servidor')
    );
  }

  private actualizarEstadoLog(status_campo, error = null) {
    this.log_ambiente.status_campo = status_campo;
    if (error != null) {
      this.log_ambiente.error = error;
    } else {
      this.log_ambiente.error = 'Se envió un null como error o error desconocido';
    }
    this.logService.actualizarCampo(this.log_ambiente).subscribe(
      resp => {
        console.log(resp);
      },
      error => {
        this.msgs1 = [{ severity: 'error', summary: '¡Error!', detail: 'Error al actualizar el log, favor de contactar a soporte.' }];
      }
    )
  }

  //validaciones
  //valida el formato del correo
  validateEmail(email): boolean {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  //valida si la desc social (empresa) ya existe en la base
  async validarEmpresa() {
    if (this.ambiente.empleadosActivos !== null) {
      await new Promise((resolve, reject) => {
        this.ambientesService.validarEmpresa(this.ambiente)
          .subscribe(resp => {
            console.log(resp);
            this.empresaError = false;
            this.detalles = undefined;
            resolve();
          }, error => {
            console.log(error);
            this.empresaError = true;
            this.detalles = error.error.error;
          });
      });
    }
  }

  //errores
  //manejador de errores
  errorHandler(error, tipo, elem = null, nombre = null) {
    this.bloqueo = false;
    this.detalles = undefined;
    if (tipo === 1) {
      console.log(error);
      this.cargando = false;
      this.detalles = 'Error en el cliente';
      this.msgs1 = [{ severity: 'error', summary: '¡Error!', detail: 'Error inesperado, favor de intentarlo más tarde.' }];
    } else if (tipo === 2) {
      console.log(error);
      if (elem != null) {
        elem.ejecutado = 2;
        elem.error = error.error.message;
      }
      this.error = true;
      if (error.error.error != undefined) {
        this.detalles = error.error.error;
      } else {
        this.detalles = error.error.message;
      }
      this.msgs1 = [{ severity: 'error', summary: '¡Error!', detail: 'Ocurrió un error en ' + nombre + ', por favor verificar y reintentar.' }];
    }

    this.actualizarEstadoLog(2, this.detalles);
  }

  //funcion para mostrar los detalles de un error tipo 2
  detallesMsg() {
    this.detail = [{ severity: 'info', summary: '¡Error!', detail: this.detalles }];
    this.detalles = undefined;
  }
}
