import {Component, HostListener, OnInit} from '@angular/core';
import {MessageService} from 'primeng/api';
import {AmbienteModel} from '../../../../models/ambiente/ambiente.model';
import {LogModel} from '../../../../models/log/log.model';
import {AmbientesService} from '../../../../services/ambientes.service';
import {LogService} from '../../../../services/log.service';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';


@Component({
  selector: 'app-girha-destruccion',
  templateUrl: './girha-destruccion.component.html',
  styleUrls: ['./girha-destruccion.component.css'],
  providers: [MessageService]
})


export class GirhaDestruccionComponent implements OnInit {

  @HostListener('window:beforeunload', ['$event'])
  beforeUnloadHander(event) {
    if (this.verificado && !this.completado && !this.correo || !this.verificado && this.completado && !this.correo || this.verificado && this.completado && !this.correo) {
      console.log(event);
      // failed attempts to prevent popup =>
      event.preventDefault();
      event.returnValue = false;
      event.stopPropagation();
    }
  }

  filteredClientes: any[];
  ambiente: AmbienteModel;
  log_ambiente: LogModel;
  serversApli: any;
  serversEngine: any;
  tables: any;
  funciones: any;
  events: any;
  sp: any;
  triggers: any;
  views: any;
  tareas: any;
  extras: any;
  verificado = false;
  progressPorcent = 0;
  total: number;
  numEjecutados = 0;
  msgs1 = [];
  detail = [];
  correos:string[] = [];
  detalles: any;
  empresaError = false;
  cargando = false;
  completado = false;
  correo = false;
  enviado = false;
  error = false;
  mostrar = false;
  limpiar = false;
  bloqueo = false;
   clientes;
  selectedCLiente=undefined;

  constructor(private ambientesService: AmbientesService, private logService: LogService, private router: Router,private messageService: MessageService,) {

  }


  ngOnInit(): void {
    this.ambiente = new AmbienteModel();
    this.log_ambiente = new LogModel();
    this.ambiente.producto = 'girha';
    this.getServidores();
    this.getAllClientes();
    this.logService.obtenerUsuario();
  }

  //funcion principal
  async nuevo(form: NgForm, tipo) {
    if (!form.invalid) {
      if (tipo === 1) {
        this.msgs1 = [];
        this.detail = [];
        this.error = false;
        this.bloqueo = true;
        this.cargando = true;
        //obtiene todos los elementos para la BD del cliente
        this.ambiente.cliente=this.selectedCLiente.code;
        this.ambiente.bd_nueva=this.selectedCLiente.name;
        this.ambiente.descEmpresa = this.selectedCLiente.descripcion_emp;
        this.cargando = false;
        this.messageService.add({key: 'c', sticky: true, severity: 'warn', summary: '¿Está seguro de desea destruir el ambiente '+this.selectedCLiente.name+' '+this.selectedCLiente.descripcion_emp +'?', detail: 'El ambiente dejara de ser accesible y los datos no se podrán recuperar.'});
        this.cargando = true;
        this.crearLog();
      }
      else if (tipo === 2) {
        this.bloqueo = true;
        if (this.ambiente.bd_nueva !== undefined) {
          await new Promise((resolve, reject) => {
            //solicita crear la BD para el cliente
            this.log_ambiente.campo = 'destruir';
            this.actualizarEstadoLog(0);
            this.ambiente.name = this.selectedCLiente.name;
            this.ambientesService.destruyeAmbiente(this.ambiente).subscribe(
              async base => {
                //this.actualizarEstadoLog(1);
                //this.ambiente.bd_nueva = base['name'];
                this.verificado = true;
                this.completado = true;
                this.correo=true;
                this.enviado = false;
                this.bloqueo = false;
                resolve();
              }, error => this.errorHandler(error, 2, null, this.ambiente.name));
          });
        }



        this.cargando = false;
        this.numEjecutados = 0;
        this.progressPorcent = 0;
        this.msgs1 = [{ severity: 'success', summary: '¡Casi completo!', detail: 'Escriba el email del cliente' }];
      }
      else if (tipo === 3) {
        this.cargando = true;
        //obtiene tareas adicionales que se requieren
        this.ambientesService.getTareasAdicionales(this.ambiente).subscribe(
          tareas => {
            this.tareas = tareas;
            this.verificado = true;
            this.cargando = false;
            this.total = this.tareas.length;
            this.numEjecutados = 0;
            this.progressPorcent = 0;
            this.msgs1 = [];
            this.detail = [];
          }, error => this.errorHandler(error, 2, null, 'Obtener tareas adicionales'));
      }
      else if (tipo === 4) {
        this.bloqueo = true;
        this.log_ambiente.campo = 'tareas';
        this.actualizarEstadoLog(0);
        this.error = false;
        this.msgs1 = [];
        this.detail = [];
        for (const elem of this.tareas) {

          if (elem.ejecutado !== -1 && elem.ejecutado !== 1) {
            this.ambiente.name = elem.tabla;
            await new Promise((resolve, reject) => {
              //define informacion default
              this.ambientesService.creaInformacionDefault(this.ambiente)
                .subscribe(resp => {
                  elem.ejecutado = 1;
                  this.actualizaProgress();
                  resolve();
                }, error => this.errorHandler(error, 2, elem, this.ambiente.name));
            });
          }
        }
        this.cargando = true;
        this.correo = true;
        await new Promise((resolve, reject) => {
          //obtiene ultimas acciones
          this.ambientesService.getUltimosPasos(this.ambiente).subscribe(
            resp => {
              console.log(resp)
              this.cargando = false;
              this.extras = resp;
              this.enviado = false;
              this.total = this.extras.length;
              this.numEjecutados = 0;
              this.progressPorcent = 0;
              this.bloqueo = false;
              this.msgs1 = [{ severity: 'success', summary: '¡Casi completo!', detail: 'El ambiente ha sido eliminado, escriba el email para notificación' }];
              resolve();
            }, error => this.errorHandler(error, 2, this.ambiente.name)
          );
        });
        this.actualizarEstadoLog(1);
      }
      else if (tipo === 5 && this.ambiente.email != null && this.ambiente.email.length>0) {
        this.cargando = false;
        this.bloqueo = true;
        this.error = false;
        this.msgs1 = [];
        this.detail = [];
        this.log_ambiente.campo = 'extras';
        this.actualizarEstadoLog(0);

        await new Promise((resolve, reject) => {
          //solicitud de correo
          this.ambientesService.envioEmailDestroy(this.ambiente).subscribe(
            resp => {

              this.enviado = true;
              this.bloqueo = false;
              this.msgs1 = [{ severity: 'success', summary: '¡Completado!', detail: '¡Los correos han sido enviados!' }];
              this.actualizaProgress();
              resolve();
              this.reiniciarValores();
            }, error => this.errorHandler(error, 2, this.ambiente.name)
          );
        });

        this.actualizarEstadoLog(1);
      } else {
        this.msgs1 = [{ severity: 'error', summary: '¡Datos incompletos!', detail: '¡Favor de verificar los campos!' }];
        this.cargando = false;
      }
    } else {
      this.msgs1 = [{ severity: 'error', summary: '¡Datos Erroneos!', detail: '¡Favor de verificar los campos!' }];
      this.cargando = false;
    }
  }

  //funciones de control interno del front
  //reinicia valores
  reiniciarValores() {
    window.location.reload();
  }

  //funcion para generar de password
  generarPass() {
    this.ambiente.password = Math.random().toString(36).substr(2, 10);
  }

  //funcion para determinar el totaal para la funcion de la barra de progreso
  igualaArray() {
    let numero = Math.max(this.tables.length, this.funciones.length, this.events.length,
      this.sp.length, this.triggers.length, this.views.length);
    let todos = [];
    todos.push(this.tables, this.funciones, this.events, this.sp, this.triggers, this.views);
    for (const elem of todos) {
      if (elem.length > 0 && elem.length < numero) {
        while (elem.length < numero) {
          elem.push({ ejecutado: -1 });
        }
      }
    }
  }

  agregarCorreo() {
    this.msgs1 = [];
    let fallidos = 0;
    this.correos = this.correos.filter(elem=>{
      if(this.validateEmail(elem)){
        return true;
      }else{
        fallidos ++;
      }
    });
    this.listaCorreos();
    console.log(this.correos);
    console.log(this.ambiente.email);
    if(fallidos > 0){
      this.msgs1 = [{ severity: 'error', summary: '¡Error!', detail: 'Ingrese un email valido, por favor verificar y reintentar.' }];
    }
  }

  listaCorreos() {
    this.ambiente.email = this.correos.join(';');
    console.log(this.ambiente.email);
    console.log(this.correos);
  }
  //funcion para mostrar el avance en la barra de progreso
  actualizaProgress() {
    this.numEjecutados++;
    this.progressPorcent = +((this.numEjecutados / this.total) * 100).toFixed(2);
  }

  //funciones para obtener los servers aplicativos y engine
  async getServidores() {
    await new Promise((resolve, reject) => {
      this.ambientesService.getServerbyProduct(this.ambiente)
        .subscribe(resp => {
          this.serversApli = resp.filter(elem => elem['tipo_conexion'] == 'MySql');
          this.serversEngine = resp.filter(elem => elem['tipo_conexion'] == 'PostgreSql');
          if (this.serversApli.length > 0 && this.serversEngine.length > 0) {
            this.mostrar = true;
          }
          resolve();
        }, error => this.errorHandler(error, 1));
    });

  }
  async getAllClientes() {
    await new Promise((resolve, reject) => {
      this.ambientesService.getAllClientes(this.ambiente)
        .subscribe(resp => {
          /*this.ambiente.cliente = resp[0].total + 1;*/
          this.clientes = resp;
          if ( this.clientes.length > 0){
            this.mostrar = true;
          }
          console.log(resp);
          resolve();
        }, error => this.errorHandler(error, 1));
    });
  }

  //funcion para actualizar el numero de cliente
  async acuatizaCliente(nombre) {
    this.ambiente.server_name = nombre;
    this.ambiente.cliente = '';
    if (nombre !== null) {
      await new Promise((resolve, reject) => {
        this.ambientesService.getLastCliente(this.ambiente)
          .subscribe(resp => {
            this.ambiente.cliente = resp[0].total + 1;
            resolve();
          }, error => this.errorHandler(error, 1));
      });
    }
  }

  //datos para log
  private crearLog() {
    this.serversApli.forEach(resServer => {
      this.log_ambiente.server_apli_id = resServer.id;
    });
    this.serversEngine.forEach(resEngine => {
      this.log_ambiente.server_engine_id = resEngine.id;
    });
    this.log_ambiente.user_id = sessionStorage.getItem('id_user');
    this.log_ambiente.producto = this.ambiente.producto;
    this.log_ambiente.cliente = this.ambiente.cliente;
    this.logService.crearLog(this.log_ambiente).subscribe(
      resp => {
        this.log_ambiente.log_id = resp['id_log'];
      },
      error => this.errorHandler(error, 2, null, 'Servidor')
    );
  }

  private actualizarEstadoLog(status_campo, error = null) {
    this.log_ambiente.status_campo = status_campo;
    if (error != null) {
      this.log_ambiente.error = error;
    } else {
      this.log_ambiente.error = 'Se envió un null como error o error desconocido';
    }
    this.logService.actualizarCampo(this.log_ambiente).subscribe(
      resp => {
        console.log(resp);
      },
      error => {
        this.msgs1 = [{ severity: 'error', summary: '¡Error!', detail: 'Error al Actualizar el log, favor de contactar a soporte.' }];
      }
    )
  }

  //validaciones
  //valida el formato del correo
  validateEmail(email): boolean {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  //valida si la desc social (empresa) ya existe en la base
  async validarEmpresa() {
    if (this.ambiente.descEmpresa !== null) {
      await new Promise((resolve, reject) => {
        this.ambientesService.validarEmpresa(this.ambiente)
          .subscribe(resp => {
            console.log(resp);
            this.empresaError = false;
            this.detalles = undefined;
            resolve();
          }, error => {
            console.log(error);
            this.empresaError = true;
            this.detalles = error.error.error;
          });
      });
    }
  }

  //errores
  //manejador de errores
  errorHandler(error, tipo, elem = null, nombre = null) {
    this.bloqueo = false;
    this.detalles = undefined;
    if (tipo === 1) {
      console.log(error);
      this.cargando = false;
      this.detalles = 'Error en el cliente';
      this.msgs1 = [{ severity: 'error', summary: '¡Error!', detail: 'Error inesperado, favor de intentarlo más tarde.' }];
    } else if (tipo === 2) {
      console.log(error);
      if (elem != null) {
        elem.ejecutado = 2;
        elem.error = error.error.message;
      }
      this.error = true;
      if (error.error.error != undefined) {
        this.detalles = error.error.error;
      } else {
        this.detalles = error.error.message;
      }
      this.msgs1 = [{ severity: 'error', summary: '¡Error!', detail: 'Ocurrió un error en ' + nombre + ', por favor verificar y reintentar.' }];
    }

    this.actualizarEstadoLog(2, this.detalles);
  }

  //funcion para mostrar los detalles de un error tipo 2
  detallesMsg() {
    this.detail = [{ severity: 'info', summary: '¡Error!', detail: this.detalles }];
    this.detalles = undefined;
  }

  onReject() {
    this.messageService.clear('c');
    this.bloqueo = false;
  }

  onConfirm(form: NgForm, tipo) {
    this.messageService.clear();
    this.nuevo(form, tipo);
    this.bloqueo = false;

  }


}

