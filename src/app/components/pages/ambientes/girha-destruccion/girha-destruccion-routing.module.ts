import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GirhaDestruccionComponent } from './girha-destruccion.component';

const routes: Routes = [{ path: '', component: GirhaDestruccionComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GirhaDestruccionRoutingModule { }
