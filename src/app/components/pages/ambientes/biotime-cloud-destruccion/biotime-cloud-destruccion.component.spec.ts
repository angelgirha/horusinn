import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BioTimeCloudDestruccionComponent } from './biotime-cloud-destruccion.component';

describe('BioTimeCloudDestruccionComponent', () => {
  let component: BioTimeCloudDestruccionComponent;
  let fixture: ComponentFixture<BioTimeCloudDestruccionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BioTimeCloudDestruccionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BioTimeCloudDestruccionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
