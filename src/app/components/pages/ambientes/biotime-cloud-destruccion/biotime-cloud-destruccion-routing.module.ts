import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BioTimeCloudDestruccionComponent } from './biotime-cloud-destruccion.component';

const routes: Routes = [{ path: '', component: BioTimeCloudDestruccionComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BioTimeCloudDestruccionRoutingModule { }
