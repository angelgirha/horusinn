import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BiotimeCloudAmbientesRoutingModule } from './biotime-cloud-ambientes-routing.module';
import { BioTimeCloudAmbientesComponent } from './biotime-cloud-ambientes.component';
import {TooltipModule} from 'primeng/tooltip';
import {TableModule} from 'primeng/table';
import {MessagesModule} from 'primeng/messages';
import {ProgressBarModule} from 'primeng/progressbar';
import {NavigationModule} from '../../../shared/navigation/navigation.module';
import {DropdownModule} from 'primeng/dropdown';
import {FormsModule} from '@angular/forms';
import {ButtonModule} from 'primeng/button';
import {InputTextModule} from 'primeng/inputtext';
import {PasswordModule} from 'primeng/password';
import {LoadingModule} from '../../../shared/loading/loading.module';
import { ChipsModule } from 'primeng/chips';

@NgModule({
  declarations: [
    BioTimeCloudAmbientesComponent,
  ],
  imports: [
    CommonModule,
    BiotimeCloudAmbientesRoutingModule,
    TooltipModule,
    TableModule,
    MessagesModule,
    ProgressBarModule,
    NavigationModule,
    DropdownModule,
    FormsModule,
    ButtonModule,
    InputTextModule,
    PasswordModule,
    LoadingModule,
    ChipsModule
  ]
})
export class BiotimeCloudAmbientesModule { }
