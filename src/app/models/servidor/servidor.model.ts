export class ServidorModel{
  id?: number;
  producto?: string;
  nombre?: string;
  ip_publica?: string;
  ip_local?: string;
  servers?: ConexionesModel[];
  error?: number;
  acciones?:any[];
}

export class ClienteDir{
  Estatus?: string;
  id_cliente?:number;
  created_at?:string;
  deleted_at?:string;
  con_dias?:number;
  rfc ?: string;
  razon_social?:string;
  nombre1?:string;
  tipo_contacto1?:string;
  correo1?:string;
  nombre2?:string;
  tipo_contacto2?:string;
  correo2?:string;
  nombre3?:string;
  tipo_contacto3?:string;
  correo3?:string;
}

export class ConexionesModel{
  id?:number
  tipo_conexion?: string;
  puerto?: number;
  usuario?: string;
  password?: string;
  centraliza?: boolean;
  dbcentral?: string;
  producto?: string;
  error?: number;
  private_key?;
  tipoPassSsh?:string;
}


