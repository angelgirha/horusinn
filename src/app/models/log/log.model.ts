export class LogModel{
    server_apli_id: number;
    server_engine_id: number;
    user_id: string;
    producto: string;
    cliente: string;
    log_id:number;
    campo: string;
    status_campo:number;
    error: string;
}